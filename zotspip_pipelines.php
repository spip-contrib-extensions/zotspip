<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function zotspip_insert_head($flux) {
	$flux .= '<link rel="stylesheet" href="' . find_in_path('css/zotspip.css') . '" type="text/css" />';
	$flux .= '<link rel="unapi-server" type="application/xml" title="unAPI" href="' . url_absolue(generer_url_public('zotspip_unapi', 'source=zotspip')) . '" />';
	return $flux;
}

function zotspip_header_prive($flux) {
	$flux .= '<link rel="stylesheet" href="' . find_in_path('css/zotspip.css') . '" type="text/css" />';
	$flux .= '<link rel="unapi-server" type="application/xml" title="unAPI" href="' . url_absolue(generer_url_public('zotspip_unapi', 'source=zotspip')) . '" />';
	return $flux;
}


// Pour passer automatiquement les [ref=...] en notes de bas de page
function zotspip_pre_typo($texte) {
	include_spip('zotspip_fonctions');
	$traiter_raccourci_ref = charger_fonction('traiter_raccourci_ref', 'zotspip');
	$texte = $traiter_raccourci_ref($texte);
	return $texte;
}

// Insertion du raccourci [ref=XXX] dans le porte-plume
function zotspip_porte_plume_barre_pre_charger($barres) {
	$barre = &$barres['edition'];

	$barre->ajouterApres('notes', [
		'id'             => 'inserer_ref',
		'name'           => _T('zotspip:outil_inserer_ref'),
		'className'      => 'outil_inserer_ref',
		'selectionType'  => '',
		'closeWith'      => '[ref=[![' . _T('zotspip:outil_explication_inserer_ref') . ' ' . _T('zotspip:outil_explication_inserer_ref_exemple') . ']!]]',
		'display'        => true
	 ]);

	return $barres;
}

// Icone pour le porte-plume
function zotspip_porte_plume_lien_classe_vers_icone($flux) {
	$icones = [];
	$icones['outil_inserer_ref'] = 'inserer_ref.png';
	return array_merge($flux, $icones);
}

// Pipeline post-boucle, utilise pour le champs annee de zitems, cf. https://core.spip.net/issues/2912

function zotspip_post_boucle($boucle) {
	if ($boucle->id_table == 'zitems') {
		$boucle->where = replaceTree('YEAR(zitems.date)', 'zitems.annee', $boucle->where);
	}
	return $boucle;
}

// Source: http://kvz.io/blog/2008/09/05/php-recursive-str-replace-replacetree/
function replaceTree($search = '', $replace = '', $array = false, $keys_too = false) {
	if (!is_array($array)) {
		// Regular replace
		return str_replace($search, $replace, $array);
	}

	$newArr = [];
	foreach ($array as $k => $v) {
		// Replace keys as well?
		$add_key = $k;
		if ($keys_too) {
			$add_key = str_replace($search, $replace, $k);
		}

		// Recurse
		$newArr[$add_key] = replaceTree($search, $replace, $v, $keys_too);
	}
	return $newArr;
}
