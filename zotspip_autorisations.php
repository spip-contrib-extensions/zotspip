<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function zotspip_autoriser() {
}

function autoriser_zotspip_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre');
}

function autoriser_zitems_menu_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

function autoriser_zotero_modifier_dist($faire, $type, $id, $qui, $opt) {
	include_spip('inc/config');
	$config = lire_config('zotspip/autoriser_modif_zotero');
	if (!$config) {
		return false;
	}
	if ($config == 'webmestre') {
		return autoriser('webmestre');
	}
	if ($config == 'admin') {
		return $qui['statut'] == '0minirezo' and !$qui['restreint'];
	}
	if ($config == 'admin_restreint') {
		return $qui['statut'] == '0minirezo' and !$qui['restreint'];
	}
	if ($config == 'redacteur') {
		return $qui['statut'] == '0minirezo' or $qui['statut'] == '1comite';
	}
	return false;
}
