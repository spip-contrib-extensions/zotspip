<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-zotspip?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// Z
	'zotspip_description' => 'Sync Spip with a Zotero library of bibliographic references (user or group library). Use Zotero to manage / import / edit your bibliographic references and insert them in your Spip using the models provided by ZotSpip.', # MODIF
	'zotspip_slogan' => 'The power of Zotero in your SPIP', # MODIF
];
