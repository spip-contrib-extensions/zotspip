<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/zotspip?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// A
	'afficher_masquer_details' => 'Exibir/ocultar os detalhes',
	'ajouter_createur' => 'Incluir outro autor',
	'ajouter_tag' => 'Incluir outra palavra-chave',
	'annee_non_precisee' => 'Ano não especificado',
	'aucune_reference' => 'Nenhuma referência corresponde.',

	// B
	'bibliographie_zotero' => 'Uma bibliografia Zotero',
	'bouton_forcer_maj_complete' => 'Forçar uma atualização completa',
	'bouton_synchroniser' => 'Sincronizar',

	// C
	'configurer_zotspip' => 'Configurar o ZotSpip',
	'confimer_remplacement' => 'Substituir <strong>@source@</strong> por <strong>@dest@</strong> ? Atenção esta operação é irreversível!',
	'confirmer' => 'Confirmar',
	'connexion_ok' => 'A conexão com o Zotero está operacional.',
	'contributeurs' => 'Contribuidores',
	'createurs' => 'Autor(es)',

	// D
	'dernieres_publications' => 'Últimas publicações',
	'description_page-biblio' => 'Pesquisa e exibição das referências bibliográficas da biblioteca Zotero sincronizadas com o ZotSpip.',
	'deselectionner_tout' => 'Desmarcar tudo',
	'droits_insuffisants' => 'Você não tem as autorizações requeridas para fazer esta alteração.',

	// E
	'erreur_connexion' => 'ZotSpip não conseguiu conectar-se com o Zotero. Verifique os seus parâmetros de conexão. Se estiver usando um proxy, verifique se ele está corretamente configurado no SPIP (Configuração > Funções avançadas). Note que o ZopSpip nem sempre funciona se um proxy for necessário.',
	'erreur_dom' => 'Para funcionar, o ZotSpip requer a extensão PHP DOM. Ative ou instale essa extensão.',
	'erreur_openssl' => 'Para funcionar, o ZotSpip requer a extensão PHP openSSL. Ative ou instale essa extensão.',
	'erreur_simplexml' => 'Para funcionar, o ZotSpip requer a extensão PHP SimpleXML. Ative ou instale essa extensão.',
	'explication_api_key' => 'Obtem-se na <a href="https://www.zotero.org/settings/keys">página do Zotero de gerenciamento de chaves pessoais</a>. Lembre-se de conceder as autorizações de acesso necessárias a essa chave.',
	'explication_autoriser_modif_zotero' => 'Ativar as opções de alteração da biblioteca Zotero (por exemplo, a fusão de autores)? Se sim, quem em as alterações necessárias para validar essas alterações? ATENÇÃO: você deve igualmente verificar se a sua <em>Chave API</em> tem autorização de escrita.',
	'explication_corriger_date' => 'O Zotero transmite as datas de publicação como foram inseridas. Portanto, o processador CSL nem sempre é capaz de as decompor corretamente devido à grande variedade de formatos. Se for esse o caso, a data de publicação não será exibida quando as referências forem formatadas. O ZotSpip pode corrigir antecipadamente as datas de publicacão. Atenção: apenas o ano será transmitido ao processador CSL, exceto se a data estiver no formato aaaa-mm-dd ou aaaa-mm. Esta opção, no entanto, não tem nenhum impacto na biblioteca Zotero em si.',
	'explication_depuis' => 'Um ano (por exemplo: <em>2009</em>) ou uma duração em anos seguida pela palavra em português <em>anos</em> (por exemplo: <em>3anos</em>) ou pela palavra em inglês <em>years</em> (por exemplo: <em>3years</em>).',
	'explication_flux_rss' => 'Acessível no endereço <em>spip.php ?page=biblio-rss</em>, este fluxo RSS apresenta as 50 inclusões mais recentes na base bibliográfica.',
	'explication_id_librairie' => 'Para uma biblioteca pessoal, o <em>userID</em> é informado na <a href="https://www.zotero.org/settings/keys">página Zotero de gerenciamento de chaves pessoais</a>. Para um grupo, o <em>groupID</em> encontra-se no URL de configuração do grupo, no formato <em>https://www.zotero.org/groups/&lt;groupID&gt;/settings</em>.',
	'explication_maj_zotspip' => 'O ZotSpip sincroniza-se em intervalos regulares (cerca de 4 horas) com o servidor Zotero. Apenas as modificações mais recentes (após a última sincronização) são consideradas. Se precisar, você pode forças uma atualização completa da base de dados, transferindo todas as referências novamente. A sincronizaçãoé realizada em várias etapas (referências, coleções Zotero, limpeza dos elementos excluídos).',
	'explication_ordre_types' => 'Você pode personalizar a ordenação por tipo de referência (altere a ordem arrastando e soltando).',
	'explication_pas_sync' => 'Máximo de 100, 50 por padrão, para as referências e 5 para as coleções. Reduza o intervalo de sincronização se encontrar erros do tipo <em>Maximum execution time</em>.',
	'explication_username' => 'Para uma biblioteca pessoal, o nome de usuário está indicado na <a href="https://www.zotero.org/settings/account">página de configuração da conta</a>. Para um grupo compartilhado, o nome do grupo encontra-se no final do URL da página de entrada do grupo, no formato <em>https://www.zotero.org/groups/&lt;nom_du_groupe&gt;</em> (em alguns casos, o nome do grupo corresponde ao seu identificador numérico).',
	'exporter' => 'Exportar',
	'exporter_reference' => 'Exportar a referência:',
	'exporter_selection' => 'Exportar a seleção no formato',

	// F
	'filtrer' => 'Filtrar',

	// I
	'identifier_via_doi' => 'Identificar o recurso a partir do seu DOI',
	'identifier_via_isbn' => 'Identificar o recurso a partir do seu ISBN',
	'item_admin' => 'administradores não-restritos',
	'item_admin_restreint' => 'todos os administradores (inclusive os restritos)',
	'item_aeres' => 'segundo a classificação AERES',
	'item_annee' => 'por ano',
	'item_annee_type' => 'por ano e, em seguida, por tipo',
	'item_aucun' => 'nenhum',
	'item_auteur' => 'por autor',
	'item_complet' => 'todos os campos',
	'item_date_ajout' => 'por data de inclusão na base',
	'item_liste' => 'lista',
	'item_liste_simple' => 'lista simples',
	'item_numero' => 'por número',
	'item_personne' => 'pessoa',
	'item_premier_auteur' => 'por autor principal',
	'item_recente' => 'publicações recentes',
	'item_redacteur' => 'administradores + redatores',
	'item_resume_tags' => 'resumo + palavras-chave',
	'item_type' => 'por tipo de referência',
	'item_type_annee' => 'por tipo e, em seguida, por ano',
	'item_type_librairie_group' => 'grupo',
	'item_type_librairie_user' => 'usuário',
	'item_updated' => 'por data de atualização',
	'item_volume' => 'por número de volume',
	'item_webmestre' => 'apenas os webmasters',
	'items_zotero' => 'Referências Zotero',

	// L
	'label_annee' => 'Ano',
	'label_api_key' => 'Chave API',
	'label_auteur' => 'Autor',
	'label_autoriser_modif_zotero' => 'Alterações da biblioteca Zotero',
	'label_collection' => 'Coleção',
	'label_conference' => 'Conferência',
	'label_corriger_date' => 'Corrigir as datas de publicação',
	'label_csl' => 'Estilo CSL (formatação)',
	'label_csl_defaut' => 'Estilo padrão',
	'label_depuis' => 'Após',
	'label_details' => 'Detalhes',
	'label_editeur' => 'Editora',
	'label_export' => 'Exibir as opções de exportação',
	'label_flux_rss' => 'Ativar o fluxo RSS ?',
	'label_id_librairie' => 'Identificador da biblioteca',
	'label_identifiants_zotero' => 'Identificadores Zotero',
	'label_liens' => 'Exibir os links?',
	'label_max' => 'Número máximo de referências exibidas',
	'label_options' => 'Opções',
	'label_options_affichage' => 'Opções de exibição',
	'label_ordre_types' => 'Ordenar por tipo de referência',
	'label_page_biblio' => 'Ativar a página ‘biblio’ para o Zpip ?',
	'label_pas_collections' => 'Sincronização de coleções',
	'label_pas_references' => 'Sincronização de referências',
	'label_pas_sync' => 'Sem sincronização',
	'label_publication' => 'Publicação',
	'label_recherche_libre' => 'Busca livre',
	'label_selection_references' => 'Seleção das referências',
	'label_souligne' => 'Sublinhar o autor principal?',
	'label_tag' => 'Palavra-chave',
	'label_tags' => 'Palavras-chave',
	'label_titre_page_biblio' => 'Título da página ‘biblio’',
	'label_tri' => 'Ordenação',
	'label_type_doc' => 'Tipo do documento',
	'label_type_librairie' => 'Tipo de biblioteca Zotero',
	'label_type_ref' => 'Tipo de referência',
	'label_username' => 'Nome do usuário ou do grupo',
	'label_variante' => 'Variante',
	'label_zcollection' => 'Coleção Zotero',
	'lien_ressource' => 'Link para o recurso',
	'liste_createurs' => 'Lista de contribuidores',
	'liste_references' => 'Lista das referências Zotero',
	'liste_tags' => 'Lista de palavras-chave',

	// M
	'maj_zotspip' => 'Atualizar o ZotSpip',
	'message_erreur_style_csl' => 'O estilo CSL @style@.csl não foi encontrado na servidor (arquivo inexistente ou plugin desativado).',
	'modifier_en_ligne' => 'Alterar online em zotero.org',

	// N
	'nettoyage' => 'limpeza',
	'nom_page-biblio' => 'Biblio',
	'nom_prenom' => 'Sobrenome, Nome',

	// O
	'outil_explication_inserer_ref' => 'Identificador Zotero da referência. No caso de uma citação, um número de página ou um número de seção pode ser especificado após o identificador, separado por @. Várias referências podem ser indicadas, separadas por vírgula.',
	'outil_explication_inserer_ref_exemple' => 'Exemplo: 4JA2I4UC@pág 16-17,FSCANX5W',
	'outil_inserer_ref' => 'Inserir uma referência bibliográfica [ref=XXX]',

	// P
	'plusieurs_collections' => '@nb@ coleções',
	'plusieurs_collections_sync' => '@nb@ coleções sincronizadas',
	'plusieurs_references' => '@nb@ referências',
	'plusieurs_references_sync' => '@nb@ referências sincronizadas',
	'probleme_survenu_lors_du_remplacement' => 'Ocorreu um problema ao substituir (code HTTP @code@).',

	// R
	'reference_num' => 'Referência n°',
	'remplacer_par' => 'Substituir por',
	'resume' => 'Resumo:',

	// S
	'sans_auteur' => 'Sem autor',
	'selectionner_tout' => 'Marcar tudo',
	'source' => 'fonte',
	'supprimer_createur' => 'Excluir este autor',
	'supprimer_tag' => 'Excluir esta palavra-chave',
	'sync_complete_demandee' => 'Uma sincronização completa da base de dados foi solicitada.',
	'sync_en_cours' => 'A sincronização está em andamento e ainda não terminou. Clique novamente em <em>Sincronizar</em>.',
	'synchronisation_effectuee' => 'Sincronização efetuada',

	// T
	'tags' => 'Palavras-chave:',
	'titre_page_biblio' => 'Referências bibliográficas',

	// U
	'une_reference' => '1 referência',

	// V
	'voir_publis_auteur' => 'Ver as publicações de @auteur@.',
	'voir_sur_zotero' => 'Consultar esta referência em zotero.org',

	// Z
	'zotspip' => 'ZotSpip',
];
