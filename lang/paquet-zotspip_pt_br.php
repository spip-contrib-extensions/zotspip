<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-zotspip?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// Z
	'zotspip_description' => 'Sincroniza o SPIP com uma biblioteca (pessoal ou coletiva) de referências bibliográficas Zotero. Use o Zotero para gerenciar / importar / redigir as suas referências bibliográficas no seu SPIP com ZotSpip através de modelos dedicados.',
	'zotspip_slogan' => 'O poder do Zotero no seu SPIP',
];
