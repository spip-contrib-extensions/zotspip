<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-zotspip?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// Z
	'zotspip_description' => 'Synchronise Spip con una biblioteca (personal o colectiva) de referencias bibliográficas Zotero. Utilice Zotero para administrar / importar / redactar sus referencias bibliográficas, luego incorpore sus referencias bibliográficas en su Spip con ZotSpip a través de modelos dedicados.', # MODIF
	'zotspip_slogan' => 'La fuerza de Zotero en su SPIP', # MODIF
];
