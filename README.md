Zotspip
=====

![zotspip](./prive/themes/spip/images/zotspip-xx.svg)

Synchronise SPIP avec une bibliothèque (personnelle ou partagée) de références bibliographiques [Zotero](https://www.zotero.org/).

- Utilisez Zotero pour gérer, importer, rédiger vos références bibliographiques
- puis incorporez vos références bibliographiques dans votre SPIP avec ZotSpip au travers d’un raccourci et de modèles dédiés.

Documentation complète :\
https://contrib.spip.net/Le-plugin-ZotSpip
